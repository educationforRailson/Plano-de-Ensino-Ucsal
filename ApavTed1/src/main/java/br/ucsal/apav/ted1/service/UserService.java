package br.ucsal.apav.ted1.service;

import java.util.List;

import br.ucsal.apav.ted1.entity.User;

public interface UserService {
	void add(User user);

	List<User> listUsers();
}
