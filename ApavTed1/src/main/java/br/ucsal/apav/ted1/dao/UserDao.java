package br.ucsal.apav.ted1.dao;

import java.util.List;

import br.ucsal.apav.ted1.entity.User;

public interface UserDao {
	void add(User user);

	List<User> listUsers();
}